from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe, Ingredient, RecipeStep
from recipes.forms import RecipeForm, IngredientForm, RecipeStepForm
from django.contrib.auth.decorators import login_required
from django.forms import modelformset_factory
# from django.contrib import messages



# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    #recipe = Recipe.objects.get(id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    
    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)

@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    
    # if request.user != recipe.author:
    #     messages.error(request, "You are not the Author!")
    #     return redirect('show_recipe', id=id)
    IngredientFormSet = modelformset_factory(
        Ingredient, form = IngredientForm, extra=1, can_delete=True,
        fields=["amount", "food_item"]
    )
    RecipeStepFormSet = modelformset_factory(
        RecipeStep, form = RecipeStepForm, extra=1, can_delete=True,
        fields=["step_number", "instruction"]
    )

    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        ingredient_formset = IngredientFormSet(request.POST, queryset=Ingredient.objects.filter(recipe=recipe), prefix= 'ingredients')
        recipestep_formset = RecipeStepFormSet(request.POST, queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')

        if form.is_valid() and ingredient_formset.is_valid() and recipestep_formset.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()

            for ingredient_form in ingredient_formset:
                if ingredient_form.cleaned_data and not ingredient_form.cleaned_data.get('DELETE'):
                    ingredient = ingredient_form.save(False)
                    ingredient.recipe = recipe
                    ingredient.save()
                elif ingredient_form.cleaned_data.get('DELETE') and ingredient_form.instance.pk:
                    ingredient_form.instance.delete()
            
            for recipestep_form in recipestep_formset:
                if recipestep_form.cleaned_data and not recipestep_form.cleaned_data.get('DELETE'):
                    recipestep = recipestep_form.save(False)
                    recipestep.recipe = recipe
                    recipestep.save()
                elif recipestep_form.cleaned_data.get('DELETE') and recipestep_form.instance.pk:
                    recipestep_form.instance.delete()

            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
        ingredient_formset = IngredientFormSet(queryset=Ingredient.objects.filter(recipe=recipe), prefix='ingredients')
        recipestep_formset = RecipeStepFormSet(queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')

    context = {
        "recipe": recipe,
        "form": form,
        "ingredient_formset": ingredient_formset,
        "recipestep_formset": recipestep_formset,
    }
    return render(request, "recipes/edit.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author = request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
